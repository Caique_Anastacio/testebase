
{
	'name' : "DietFatcs",
	'version' : "1.0",
	'author' : "CBLOL, Agoravai.com",
	'description' : 'Adds nutrition information to products',
	'depends' : ['sale'],
	'data' : ['dietfacts_view.xml','security/ir.model.access.csv'],
	'installable' : True,
}
